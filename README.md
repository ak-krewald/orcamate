orcamate
========


`orcamate.data`
---------------

Initialize a `Calculation` object with the ORCA output file:

```py
calculation = orcamate.data.Calculation("path/to/orca_outputfile.out")
```


`orcamate.data.Calculation`
---------------------------

The `Calculation` object has multiple self-explainatory properties:

- `output_file`, `gbw_file`, `cis_file`, `xyz_file` as `pathlib.Path` objects
- `input_file_contents`, `package_version`, and `keywords` as strings
- `mult`, `charge`, `n_atoms`, `n_basis` as integers
- `electronic_transitions` as a dict
- `final_single_point_energy` as `quantiphy.Quantity` object


`quantiphy.Quantity`
--------------------

A quantity describes a value and a unit, and that's exactly what the `quantiphy.Quantity` does.

The object subclasses the `float` object, so it works like one in native ways.

The units can be interconverted, the default is hartree (Eh).

```py
value_in_kjmol = quantiphy.Quantity(calculation.final_single_point_energy, scale='kJ/mol')

print(value_in_kjmol)
>>> -144 kJ/mol

print(value_in_kjmol.real)
>>> -144
```

Available units:

- `Eh`, `kJ/mol`, `kcal/mol`, `eV`, `cm-1`


`orcamate.surface_scan`
-----------------------

Visualize an ORCA SOC-TDDFT potential energy surface scan.

The workflow is to generate the relaxed geometries with a scan and then compute the SOC-TDDFT
spectra on these geometries.
This way the SOC-TDDFT calculations are numbered with a `.001` suffix.

Initialize an `orcamate.surface_scan.SurfaceScan` object with a list of all SOC-TDDFT output files:

```py
from pathlib import Path
from orcamate.surface_scan import SurfaceScan

single_points = Path("path/to/single_points").rglob('*.out')
scan = SurfaceScan(single_points)
```

You can then plot the surface scan with Plotly:

```py
import plotly.graph_objects as go

def x_scaler(x: int) -> float:
    return (x + 109) / 100

fig = scan.plot(n_states=32, x_scaler=x_scaler)
fig.update_layout(title="My Surface Scan", width=640, height=480)
fig.show()
```

The `x_scaler` parameter is a function that converts the scan step id (e.g. `001` to `101` for 101 scan steps)
to the desired unit. By default the x axis simply shows the scan step ids.

In the example above, the bond length scan was performed from 1.10 Å to 2.10 Å in 101 steps.
To convert the scan step id to a bond length, we add `109` to the step id (`(x + 109)`) and then
divide by `(x + 109) / 100` to get the bond length in Å.


`orcamate.surface_scan.SinglePoint`
-----------------------------------

The `SinglePoint` class subclasses the `orcamate.data.Calculation` object and extends it with:

- the scan step id (extracted from the filename)
- the spin-orbit coupled electronic transitions ("SPIN ORBIT CORRECTED ABSORPTION SPECTRUM VIA TRANSITION ELECTRIC DIPOLE MOMENTS")
- a "less than" (`__lt__`) implementation that allows sorting of `SinglePoint` instances by their scan step id


`orcamate.surface_scan.SurfaceScan`
-----------------------------------

The `SurfaceScan` object is initialized with a list of `orcamate.surface_scan.SinglePoint` objects.

The `SinglePoint` objects are sorted and their relative energies are calculated and added to each `SinglePoint`
as a new property `SinglePoint.final_single_point_energy_relative`.

The method `SurfaceScan.trace(state_id, x_scaler)` compiles and returns a `go.Scatter` trace of the specified state.

`SurfaceScan.plot(n_states, x_scaler)` returns a `go.Figure` object with the specified number of states.
