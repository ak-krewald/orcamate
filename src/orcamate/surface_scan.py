from pathlib import Path

import quantiphy as qt
import plotly.graph_objects as go

from . import parsing
from . import data
from . import quick_plot as qp

class SinglePoint(data.Calculation):

    def __init__(self, output_file: str, id: int=1) -> None:
        super().__init__(output_file)
        self.id: int = id
        self.states: list[parsing.SOCEigenvector] = parsing.parse_soc_matrix(self.output_file)
        
    def __lt__(self, other) -> bool:
        return self.id < other.id

    @property
    def electronic_transitions_soc(self) -> dict[list]:
        """Retrieve electronic transitions data.

        Returns:
            dict[list]: Dictionary with keys 'energies' and 'osc_strengths'.
        """
        spectrum: list[list[float]] = self.cclib_data.transprop["SPIN ORBIT CORRECTED ABSORPTION SPECTRUM VIA TRANSITION ELECTRIC DIPOLE MOMENTS"]
        return {
            'energies': [qt.Quantity(x, units='Eh') for x in spectrum[0]],
            'osc_strengths': [y for y in spectrum[1]]
        }
    

class SurfaceScan:
    
    def __init__(self, single_points: list[Path], reference_energy_eh: float=None) -> None:
        self.single_points: list[SinglePoint] = []
        for id, sp in enumerate(single_points):
            self.single_points.append(SinglePoint(sp, id))
        self.compute_relative_energies(self.single_points, reference_energy_eh)

    def compute_relative_energies(self, single_points: list[SinglePoint], reference_energy_eh: float=None) -> None:
        """Add relative energy property to each SinglePoint object.
        
        Uses either supplied reference energy (in Eh) or the lowest energy single point.
        
        Modifies the SingelPoint objects in-place, so returns None.
        """
        reference_fspe: float = self.find_first_local_minimum().final_single_point_energy if reference_energy_eh is None else reference_energy_eh
        for sp in single_points:
            sp.final_single_point_energy_relative = qt.Quantity(sp.final_single_point_energy - reference_fspe, units='Eh') 

    def find_first_local_minimum(self) -> SinglePoint:
        """Get the SinglePoint of the first local final single point energy minimum."""
        for id, sp in enumerate(self.single_points):
            if sp.final_single_point_energy < self.single_points[id+1].final_single_point_energy:
                return sp

    def trace(self, state_id: int, x_scaler: callable = lambda x: x, colorscale: str="SOC") -> go.Scatter:
        """Generate a Plotly Scatter object for a specific electronic state.

        Args:
            state_id (int): ID of the electronic state.
            x_scaler (callable, optional): Function to scale x-values. Defaults to identity function.

        Returns:
            go.Scatter: Plotly Scatter object.
        """
        energies = []
        singlet_projection = []
        for sp in self.single_points:
            state_energy = sp.electronic_transitions_soc['energies'][state_id - 1] if state_id > 0 else 0
            singlet_projection.append(sum([element.weight for element in sp.states[state_id].elements if element.spin == 0]))
            relative_energy = qt.Quantity(state_energy + qt.Quantity(sp.final_single_point_energy_relative, scale='cm-1'), units='cm-1', scale='kJ/mol')
            energies.append(relative_energy)

        x = [x_scaler(x) for x in range(len(energies))]
        y = energies
        trace = qp.scatter(x, y)
        
        state_ids = [state_id for _ in range(len(energies))]
        singlet_projections = singlet_projection[:len(energies)]
        triplet_projections = [1 - weight for weight in singlet_projection[:len(energies)]]
        customdata = list(zip(state_ids, singlet_projections, triplet_projections))
        hovertemplate = '<i>State %{customdata[0]}</i> | %{x:.2f}<br>' \
                        + 'Energy: %{y:.1f} kJ/mol<extra></extra><br>' \
                        + 'S %{customdata[1]:.0%} | T %{customdata[2]:.0%}'

        colorbar = dict(title="%Triplet", ticks="outside", tickvals=[0, 0.5, 1], ticktext=["0 %", "50 %", "100 %"])

        marker = dict(
            color = [1-weight for weight in singlet_projection[:len(energies)]],
            colorscale = qp.COLORSCALES[colorscale],
            cmin = 0,
            cmax = 1,
            showscale=True,
            colorbar=colorbar,
            opacity=0.66
        )

        trace.update(dict(customdata=customdata, hovertemplate=hovertemplate, marker=marker))

        return trace
    
    def plot(self, n_states: int = 1, x_scaler: callable = lambda x: x, title: str="Surface Scan", colorscale: str="SOC") -> go.Figure:
        fig = qp.create_fig(title=title)

        for state_id in range(n_states):
            fig.add_trace(self.trace(state_id, x_scaler, colorscale))

        return fig