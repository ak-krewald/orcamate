from dataclasses import dataclass
from pathlib import Path
import re

@dataclass
class Element:
    weight: float
    real: float
    imag: float
    root: int
    spin: float
    m_s: int

@dataclass
class SOCEigenvector:
    elements: list[Element]

def parse_soc_matrix(output_file: Path) -> list[SOCEigenvector]:
    eigenvectors = []
    elements: list[tuple[float]] = []
    if not isinstance(output_file, Path):
        output_file = Path(output_file)
    file_contents = output_file.read_text()

    soc_state_regex = re.compile(r"STATE\s*(?P<id>\d+):\s*(?P<energy_cm>\d+\.\d+)\n(?P<contributions>(?:(?!\n\n)[0-9.: \-\n])+)")
    soc_states = [match.groupdict() for match in re.finditer(soc_state_regex, file_contents)]

    for state in soc_states:
        contributions = state['contributions'].replace(':', '').strip().split('\n')
        elements = [Element(*map(float, contrib.split())) for contrib in contributions]
        eigenvectors.append(SOCEigenvector(elements))
        
    return eigenvectors


