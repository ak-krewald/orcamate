from pathlib import Path
import re

import cclib
import quantiphy as qt

eh_kjmol = qt.UnitConversion('kJ/mol', 'Eh', 2.6255e3)
eh_kcalmol = qt.UnitConversion('kcal/mol', 'Eh', 6.27509e2)
eh_ev = qt.UnitConversion('eV', 'Eh', 2.72114e1)
eh_cm = qt.UnitConversion('cm-1', 'Eh', 2.194746e5)
cm_kjmol = qt.UnitConversion('kJ/mol', 'cm-1', 1.19627e-2)

class Calculation:

    def __init__(self, output_file: str) -> None:
        self.output_file: Path = Path(output_file)
        assert self.output_file.exists(), f"Specified output file does not exist!"
        self.cclib_data = cclib.io.ccread(self.output_file)

        self.gbw_file: Path = self.output_file.with_suffix('.gbw')
        self.cis_file: Path = self.output_file.with_suffix('.cis')

        self.input_tokens: str = self.cclib_data.metadata['input_file_contents'].split()
        self.package_version: str = self.cclib_data.metadata['package_version']
        self.keywords: list[str] = self.cclib_data.metadata['keywords']
        self.mult: int = self.cclib_data.mult
        self.charge: int = self.cclib_data.charge
        self.n_atoms: int = self.cclib_data.natom
        self.n_basis: int = self.cclib_data.nbasis

    def __repr__(self) -> str:
        return f"Calculation({self.output_file.name})"

    @property
    def xyz_file(self) -> Path:
        xyz_file = self.regex_search(r"^\*.*\b(\w+\.xyz)")
        return Path(xyz_file)

    @property
    def final_single_point_energy(self) -> qt.Quantity:
        """Obtain the 'FINAL SINGLE POINT ENERGY' quantity.
        
        quantiphy.Quantity subclasses float, use quantiphy.Quantity(x, scale='kJ/mol')
        to interconvert units.

        Available units: Eh, kJ/mol, kcal/mol, eV, cm-1
        """
        result  = self.regex_search(r"FINAL SINGLE POINT ENERGY\s+(-?\d+\.\d+)")
        return qt.Quantity(result, units='Eh')
    
    @property
    def electronic_transitions(self) -> dict[list]:
        """Electronic transitions as parsed by cclib."""
        return {
            'energies': [qt.Quantity(value=e, units='cm-1') for e in self.cclib_data.etenergies],
            'osc_strengths': self.cclib_data.etoscs
        }
    
    @property
    def nroots(self) -> int | None:
        """Number of roots (nroots)."""
        nroots = self.regex_search(r"(?i)(?:\bnroots\s+(\w+))")
        if nroots:
            return int(nroots)
        return None
        
    @property
    def do_soc(self) -> bool:
        """Do a spin-orbit coupled calculation (doSOC)."""
        do_soc = self.regex_search(r"(?i)(?:\bdoSOC\s+(\w+))") or False
        return bool(do_soc)

    @property
    def do_nto(self) -> bool:
        """Print natural transition orbitals (doNTO)."""
        do_nto = self.regex_search(r"(?i)(?:\bdoNTO\s+(\w+))") or False
        return bool(do_nto)
    
    @property
    def tda(self) -> bool:
        """Tamm-Dancoff approximation (TDA)."""
        tda = self.regex_search(r"(?i)(?:\bTDA\s+(\w+))") or True
        return bool(tda)
        
    def regex_search(self, pattern: str) -> str | None:
        """Compile the RegEx pattern and return the first match found in the output file."""
        pattern = re.compile(pattern)
        result = re.search(pattern, self.output_file.read_text())
        if result is not None:
            return result.groups()[0]
        return None
