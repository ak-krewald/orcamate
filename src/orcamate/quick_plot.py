"""Wrapper for common Plotly plotting needs."""
import plotly.graph_objects as go

COLORSCALES = {
    "SOC": [[0, '#3030BF'], [0.5, '#ff00ff'], [1, '#BF3030']],
    "SOC muted": [[0, '#8F8FBF'], [0.5, '#ff00ff'], [1, '#BF8F8F']]
}

def create_fig(title: str ="", xlabel: str="x", ylabel: str="y", ) -> go.Figure:
    fig = go.Figure()
    fig.update_layout(
        title=title,
        showlegend = False,
        font = {'family': 'arial', 'size': 18},
        template='simple_white',
        plot_bgcolor='rgb(246, 246, 246)'
    )
    # Add axis labels, borders, and gridlines.
    fig.update_xaxes(title=xlabel, mirror=True, showgrid=True)
    fig.update_yaxes(title=ylabel, mirror=True, showgrid=True)
    return fig

def scatter(x: list, y: list) -> go.Scatter:
    trace = go.Scatter(x=x, y=y, mode="markers")
    return trace

def scatter_plot(x: list, y: list, title: str="Scatter", xlabel: str="x", ylabel="y") -> go.Figure:
    fig = create_fig(title=title, xlabel=xlabel, ylabel=ylabel)
    trace = scatter(x=x, y=y)
    fig.add_trace(trace)
    return fig