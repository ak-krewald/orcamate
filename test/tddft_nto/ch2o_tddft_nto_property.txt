-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -114.2814457384
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                  7.9999993437 
   Number of Beta  Electrons                  7.9999993437 
   Total number of  Electrons                15.9999986875 
   Exchange energy                          -10.5339027047 
   Correlation energy                        -0.5205880090 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -11.0544907136 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -114.2814457384 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 4
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 3
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8321     6.0000     0.1679     3.9513     3.9513    -0.0000
  1   0     8.2479     8.0000    -0.2479     2.1647     2.1647    -0.0000
  2   0     0.9600     1.0000     0.0400     0.9561     0.9561     0.0000
  3   0     0.9600     1.0000     0.0400     0.9561     0.9561     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.096618
                0             6               2            1                0.927344
                0             6               3            1                0.927325
# -----------------------------------------------------------
$ CIS_ABS
   description: The CIS absorption spectrum 
   geom. index: 1
   prop. index: 1
        Number of roots:  5
        The CIS Absorption Spectrum :
                  0          1          2          3          4          5    
      0       0.142625   0.000000   0.000000  -0.000002  -0.000022  -0.000099
      1       0.315050   0.126205   0.600882  -0.085855   0.641188  -0.427070
      2       0.327699   0.001252   0.005733  -0.005622   0.041420   0.063129
      3       0.352966   0.017244   0.073282   0.268346   0.035670   0.000313
      4       0.384542   0.000000   0.000000   0.000018  -0.000296   0.000341
        The CIS Absorption Spectrum (Velocity):
                  0          1          2          3          4          5    
      0       0.142625   0.000000   0.000000  -0.000006  -0.000028  -0.000010
      1       0.315050   0.106883   0.050510   0.024899  -0.185899   0.123824
      2       0.327699   0.004872   0.002395   0.003634  -0.026734  -0.040825
      3       0.352966   0.006843   0.003623  -0.059677  -0.007852  -0.000103
      4       0.384542   0.000000   0.000000   0.000041   0.000118  -0.000149
# -----------------------------------------------------------
$ CIS_CD
   description: The CIS CD spectrum 
   geom. index: 1
   prop. index: 1
        Number of roots:  5
        The CIS CD Spectrum:
                  0          1          2          3    
      0      -0.001439   0.606144   0.081407   0.000071
      1       0.042049   0.031019  -0.231565  -0.354108
      2      -0.040124   0.087522  -0.651517   0.433921
      3       0.008668   0.000105  -0.000279   0.000114
      4       0.003446  -0.343903  -0.046496  -0.000248
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0007193042
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       4
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             38
     number of aux C basis functions:       0
     number of aux J basis functions:       120
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -114.139540
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : ch2o_tddft_nto.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        2.3109486679
        Electronic Contribution:
                  0    
      0       1.239000
      1       0.166434
      2       0.000076
        Nuclear Contribution:
                  0    
      0      -2.140097
      1      -0.287389
      2       0.000000
        Total Dipole moment:
                  0    
      0      -0.901097
      1      -0.120955
      2       0.000076
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    4 
    Geometry Index:     1 
    Coordinates:
               0 C     -2.732080000000    0.700970000000    0.000000000000
               1 O     -1.523480000000    0.863270000000   -0.000000000000
               2 H     -3.373780000000    1.405540000000   -0.517470000000
               3 H     -3.165160000000   -0.147930000000    0.517470000000
